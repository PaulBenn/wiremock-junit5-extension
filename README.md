# WireMock JUnit 5 Extension

[WireMock](http://wiremock.org) integration with the [JUnit 5 Jupiter](https://junit.org/junit5/) programming model.

* Functions in much the same way as JUnit 4's [`@Rule`](https://junit.org/junit4/javadoc/4.12/org/junit/Rule.html).
* Full range of WireMock configuration supported.
* Injectable WireMock base URLs via Jupiter's parameter resolver.
* Uses the [Extension](https://junit.org/junit5/docs/current/user-guide/#extensions) framework to integrate seamlessly with existing code.

Latest stable version is **0.1.0**.
#### Maven dependency
```xml
<dependency>
    <groupId>uk.co.paulbenn</groupId>
    <artifactId>wiremock-junit5-extension</artifactId>
    <version>${wiremock-junit5-extension.version}</version>
</dependency>
```

#### Gradle dependency
```groovy
testImplementation 'uk.co.paulbenn:wiremock-junit5-extension:$wireMockJUnit5Version'
```

## Usage
1. Add the extension to the test class's [`@ExtendWith`](https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/extension/ExtendWith.html) clause.
2. Annotate any `String` parameter in any JUnit-managed method (e.g. `@Test` or `@BeforeEach`) with `@WireMockBaseUrl`. Jupiter's parameter resolution will replace the annotated parameter's value with the base WireMock URL of the selected server, complete with scheme and port.

Example:
```java
@ExtendWith(WireMockExtension.class)
class MyTests {

    @Test
    void myTest(@WireMockBaseUrl final String wireMockBaseUrl) {
        // resolved to e.g. "http://localhost:45678"
        this.wireMockBaseUrl = wireMockBaseUrl;
    }
}
```

### The default server
If there are no `@Named` fields of type `WireMockServer` on the test class, the extension will register a default server with the following properties:
* The name `default` (used for internal reference).
* Loop-back network interface's host name, currently hardcoded to `localhost`.
* Dynamic HTTP port: the default server is not capable of mocking HTTPS traffic.
* All other options come from `WireMockConfiguration::options()`.

If there is at least one `@Named` server field, this server will not be registered.

### Explicitly declared `WireMockServer` instances
To let the extension manage the lifecycle of WireMock servers declared in a test class, you **must** annotate the relevant fields with `@Named`. Example:
```java
@Named("foo")
private WireMockServer fooMockServer;
@Named("bar")
private WireMockServer barMockServer;

@Test
void myTest(@WireMockBaseUrl("foo") final String wireMockBaseUrl) {
    // ...
}
```

* Fields of any other type (not `WireMockServer`) will be ignored.
* Fields without the `@Named` annotation will be ignored.
* Two servers `@Named` in the same way will produce an error.

## Migration from JUnit 4
1. Update your `pom.xml` or `build.gradle` file to include the JUnit 5 dependencies (see [JUnit Jupiter on Maven Central](https://mvnrepository.com/artifact/org.junit.jupiter)).
2. Remove all `@Rule` and `@ClassRule` annotations.
3. Add the `@ExtendWith(...)` annotation to the required test classes.
4. Inject the server URL using `@WireMockBaseUrl` wherever needed.

---
For licensing information, see `LICENSE.md`. All code signed by publicly available OpenPGP key under author's email address.
