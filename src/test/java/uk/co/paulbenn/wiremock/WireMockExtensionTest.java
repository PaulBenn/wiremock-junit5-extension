package uk.co.paulbenn.wiremock;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.util.Scanner;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(WireMockExtension.class)
class WireMockExtensionTest {

    private static final String CONTENT = "CONTENT";

    private String readContent(String url) {
        try {
            Scanner scanner = new Scanner(new URL(url).openStream()).useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Nested
    class DefaultServerRegistrationTest {
        private String defaultServerUrl;

        @BeforeEach
        void setUp(@WireMockBaseUrl final String defaultServerUrl) {
            this.defaultServerUrl = defaultServerUrl;
        }

        @Test
        void stubsSuccessfully() {
            stubFor(get("/").willReturn(aResponse().withBody(CONTENT)));
            assertEquals(CONTENT, readContent(defaultServerUrl));
        }

        @Test
        void verifiesSuccessfully() {
            stubFor(get("/").willReturn(aResponse().withBody(CONTENT)));
            readContent(defaultServerUrl);
            verify(1, getRequestedFor(urlPathEqualTo("/")));
        }
    }

    @Nested
    class ExplicitServerRegistrationTest {
        @Named("foo")
        private final WireMockServer fooServer = new WireMockServer(WireMockConfiguration.options().dynamicPort());
        @Named("bar")
        private final WireMockServer barServer = new WireMockServer(WireMockConfiguration.options().dynamicPort());

        @Test
        void fooAndBarHAveDifferentUrls(
                @WireMockBaseUrl("foo") final String fooServerUrl,
                @WireMockBaseUrl("bar") final String barServerUrl) {
            assertNotEquals(fooServerUrl, barServerUrl);
        }

        @Test
        void fooStubsSuccessfully(@WireMockBaseUrl("foo") final String fooServerUrl) {
            fooServer.stubFor(get("/").willReturn(ok().withBody(CONTENT)));
            assertEquals(CONTENT, readContent(fooServerUrl));
        }

        @Test
        void fooVerifiesSuccessfully(@WireMockBaseUrl("foo") final String fooServerUrl) {
            fooServer.stubFor(get("/").willReturn(ok().withBody(CONTENT)));
            readContent(fooServerUrl);
            fooServer.verify(1, getRequestedFor(urlPathEqualTo("/")));
        }

        @Test
        void barStubsSuccessfully(@WireMockBaseUrl("bar") final String barServerUrl) {
            barServer.stubFor(get("/").willReturn(ok().withBody(CONTENT)));
            assertEquals(CONTENT, readContent(barServerUrl));
        }

        @Test
        void barVerifiesSuccessfully(@WireMockBaseUrl("bar") String barServerUrl) {
            barServer.stubFor(get("/").willReturn(ok().withBody(CONTENT)));
            readContent(barServerUrl);
            barServer.verify(1, getRequestedFor(urlPathEqualTo("/")));
        }
    }

    @Nested
    class MappingsResetBetweenTests {
        private String defaultServerUrl;

        @BeforeEach
        void setUp(@WireMockBaseUrl String defaultServerUrl) {
            this.defaultServerUrl = defaultServerUrl;
        }

        @Test
        @Order(1)
        void whenOneTestSetsUpAStub() {
            stubFor(get("/").willReturn(aResponse().withBody(CONTENT)));
            assertEquals(CONTENT, readContent(defaultServerUrl));
        }

        @Test
        @Order(2)
        void thenTheNextTestDoesNotSeeIt() {
            UncheckedIOException uncheckedIOException = assertThrows(
                    UncheckedIOException.class,
                    () -> readContent(defaultServerUrl)
            );

            // readContent() throws FileNotFoundException on 404 instead of an HTTP-related exception. :(
            assertTrue(uncheckedIOException.getCause() instanceof FileNotFoundException);
        }
    }
}
