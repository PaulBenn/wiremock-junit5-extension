package uk.co.paulbenn.wiremock;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The WireMock JUnit 5 extension simplifies the lifecycle management of WireMock servers in Jupiter test suites.
 *
 *  <p>Example usage:
 *
 *  <pre>
 *  &#64;ExtendWith(WireMockExtension.class)
*   class MyTest {
 *      // ...
 *  }
 *  </pre>
 *
 * <b>Before</b> each test, this extension will:
 * <ul>
 *     <li>Discover all explicitly declared {@code WireMockServer} fields on the test class. If none are found, the
 *     extension will create a default server specifying a dynamically resolved HTTP port.</li>
 *     <li>Resolve the runtime values of all declared fields against the test instance, once it exists.</li>
 *     <li>Start all resolved servers, including the default instance if applicable, thus making their base URLs
 *     available to query.</li>
 * </ul>
 *
 * <b>After</b> each test, this extension will:
 * <ul>
 *     <li>Reset all relevant properties on all resolved servers, including the default instance if applicable. This
 *     encompasses the usual reset of all stub mappings, clearing the request history and restoring any default
 *     mappings.</li>
 * </ul>
 *
 * <p>{@code WireMockExtension} simulates the behaviour of the JUnit 4 WireMock {@code @Rule}, and can almost always be
 * used as a drop-in replacement when upgrading to Jupiter.
 */
public class WireMockExtension implements BeforeEachCallback, AfterEachCallback, ParameterResolver {

    /**
     * Extension-specific name for the default server instance. The default server is only created when the test class
     * does not explicitly declare any {@code WireMockServer} fields. It is roughly equivalent to {@link WireMock}'s
     * {@code defaultInstance}, and uses a dynamic HTTP port. The default server does not support HTTPS.
     */
    public static final String DEFAULT_SERVER_NAME = "default";

    private static final Logger LOGGER = LoggerFactory.getLogger(WireMockExtension.class);

    private final ExtensionContext.Namespace contextNamespace;

    /**
     * No-args constructor required by {@link Extension} implementations.
     */
    public WireMockExtension() {
        this.contextNamespace = ExtensionContext.Namespace.create(this.getClass());
    }

    /**
     * Discovers WireMock servers (setting up a default if necessary) and starts them all.
     *
     * @param context the extension context
     * @see WireMockServer#start()
     */
    @Override
    public void beforeEach(ExtensionContext context) {
        Object testClassInstance = context.getRequiredTestInstance();

        Map<String, Field> serverFields = discoverServerFields(context.getRequiredTestClass());
        Map<String, WireMockServer> servers = resolveServerFields(serverFields, testClassInstance);

        LOGGER.debug("Discovered {} explicitly declared WireMock server(s)", servers.size());

        if (servers.isEmpty()) {
            LOGGER.debug("Registering default WireMock server '{}' (HTTP-only)", DEFAULT_SERVER_NAME);

            WireMockServer defaultServer = new WireMockServer(WireMockConfiguration.options().dynamicPort());
            servers.put(DEFAULT_SERVER_NAME, defaultServer);

            LOGGER.debug("Registered default WireMock server '{}'", DEFAULT_SERVER_NAME);
        }

        for (var namedServer : servers.entrySet()) {
            String name = namedServer.getKey();
            WireMockServer server = namedServer.getValue();

            startServer(name, server);
            getStore(context).put(StoreKeys.wiremockServerKey(name), server.baseUrl());

            if (name.equals(DEFAULT_SERVER_NAME)) {
                WireMock.configureFor("localhost", server.port());
                LOGGER.debug("[WM-server: {}] Static access enabled", DEFAULT_SERVER_NAME);
            } else {
                Reflection.writeField(serverFields.get(name), testClassInstance, server);
                LOGGER.debug("[WM-server: {}] Written to test instance", name);
            }
        }

        getStore(context).put(StoreKeys.KNOWN_SERVERS_KEY, servers);
    }

    /**
     * Resets the state of all known WireMock servers, clearing all stub mappings.
     *
     * @param context the extension context
     * @see WireMockServer#resetAll()
     */
    @Override
    public void afterEach(ExtensionContext context) {
        Object serversRaw = getStore(context).get(StoreKeys.KNOWN_SERVERS_KEY);

        @SuppressWarnings("unchecked")
        Map<String, WireMockServer> servers = (Map<String, WireMockServer>) serversRaw;

        for (var namedServer : servers.entrySet()) {
            resetServer(namedServer.getKey(), namedServer.getValue());
        }
    }

    /**
     * Returns {@code true} if the parameter is a string annotated with {@link WireMockBaseUrl}, {@code false}
     * otherwise.
     *
     * @param parameterContext the parameter context
     * @param extensionContext the extension context (unused)
     * @return {@code true} if the parameter is a string annotated with {@link WireMockBaseUrl}, {@code false}
     * otherwise.
     */
    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return parameterContext.getParameter().isAnnotationPresent(WireMockBaseUrl.class)
                && String.class.isAssignableFrom(parameterContext.getParameter().getType());
    }

    /**
     * Resolves strings annotated with {@link WireMockBaseUrl} to a corresponding WireMock server's base URL.
     *
     * @param parameterContext the parameter context
     * @param extensionContext the extension context
     * @return a WireMock server's base URL
     */
    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        String serverName = parameterContext
                .findAnnotation(WireMockBaseUrl.class)
                .map(WireMockBaseUrl::value)
                .orElseThrow(
                        // should never happen due to 'supportsParameter' above
                        () -> new WireMockExtensionException(
                                String.format(
                                        "Parameter '%s' missing required annotation @%s",
                                        parameterContext.getParameter().getName(),
                                        WireMockBaseUrl.class.getSimpleName()
                                )
                        )
                );

        Object baseUrl = getStore(extensionContext).get(StoreKeys.wiremockServerKey(serverName));

        if (baseUrl == null) {
            reportUnknownServerName(serverName, extensionContext);
        }

        return baseUrl;
    }

    private void startServer(String name, WireMockServer server) {
        LOGGER.debug("[WM-server: {}] Starting...", name);

        if (!server.isRunning()) {
            server.start();
        }

        LOGGER.debug("[WM-server: {}] Started (URL: '{}')", name, server.baseUrl());
    }

    private void resetServer(String name, WireMockServer server) {
        if (server == null || !server.isRunning()) {
            return;
        }

        LOGGER.debug("[WM-server: {}] Resetting...", name);
        server.resetAll();
    }

    private ExtensionContext.Store getStore(ExtensionContext extensionContext) {
        return extensionContext.getStore(this.contextNamespace);
    }

    private Map<String, Field> discoverServerFields(Class<?> testClass) {
        List<Field> wireMockServerFieldList = Arrays.stream(testClass.getDeclaredFields())
                .filter(f -> WireMockServer.class.isAssignableFrom(f.getType()))
                .collect(Collectors.toList());

        Map<String, Field> serverFields = new HashMap<>();

        for (Field wireMockServerField : wireMockServerFieldList) {
            Named named = wireMockServerField.getAnnotation(Named.class);

            if (named == null) {
                reportMissingNamedAnnotation(wireMockServerField);
                continue;
            }

            String name = named.value();

            if (serverFields.containsKey(name)) {
                reportNameClash(name, wireMockServerField);
            }

            LOGGER.debug("Discovered WireMock server '{}'", name);
            serverFields.put(named.value(), wireMockServerField);
        }

        return serverFields;
    }

    private Map<String, WireMockServer> resolveServerFields(Map<String, Field> serverFields, Object testClassInstance) {
        Map<String, WireMockServer> wireMockServers = new HashMap<>();
        for (var namedServer : serverFields.entrySet()) {
            WireMockServer wireMockServer = Reflection.readField(namedServer.getValue(), testClassInstance);
            wireMockServers.put(namedServer.getKey(), wireMockServer);
        }
        return wireMockServers;
    }

    // error reporting

    private void reportMissingNamedAnnotation(Field wireMockServerField) {
        LOGGER.warn(
                "Discovered WireMock server '{}' without a @{} annotation. "
                        + "This server will NOT be managed by the extension.",
                wireMockServerField.getName(),
                Named.class.getSimpleName()
        );
    }

    private void reportNameClash(String clashingName, Field problematicField) {
        String message = String.format(
                "More than 1 WireMock server is @%s '%s' (second occurrence annotates field '%s')",
                Named.class.getSimpleName(),
                clashingName,
                problematicField.getName()
        );

        LOGGER.error(message);

        throw new WireMockExtensionException(message);
    }

    private void reportUnknownServerName(String serverName, ExtensionContext context) {
        Object serversRaw = getStore(context).get("WM-known-servers");

        @SuppressWarnings("unchecked")
        Map<String, WireMockServer> servers = (Map<String, WireMockServer>) serversRaw;

        String message = String.format(
                "Annotated parameter references unknown WireMock server '%s'. Known servers: %s",
                serverName,
                servers == null
                        ? "[]"
                        : servers.keySet().stream().map(s -> "'" + s + "'").collect(Collectors.toSet())
        );

        LOGGER.error(message);

        throw new WireMockExtensionException(message);
    }
}
