package uk.co.paulbenn.wiremock;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Parameter resolution annotation.
 *
 * Method parameters of type {@link String} annotated with {@code WireMockBaseUrl} will resolve to the base URL of the
 * WireMock server referenced by the annotation's {@code value}. Omitting the value will result in the URL of the
 * default server, if no servers are explicitly declared in the test class.
 *
 * <p>Example usage:
 *
 * <pre>
 * &#64;BeforeEach
 * void setUp(&#64;WireMockBaseUrl final String wireMockUri) {
 *     this.testComponent = new TestComponent(wireMockUri);
 * }
 * </pre>
 *
 * @see ParameterResolver
 * @see WireMockServer#baseUrl()
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface WireMockBaseUrl {
    /**
     * A {@code WireMockServer}'s name (set on the field itself, via {@link Named}). The value of the resolved parameter
     * will be the base URL of the referenced server. Omitting the value will result in the URL of the default server.
     *
     * @return the base URL of the WireMock server referenced by {@code value}
     */
    String value() default WireMockExtension.DEFAULT_SERVER_NAME;
}
