package uk.co.paulbenn.wiremock;

/**
 * Signals a non-transient, fatal error in the discovery, resolution or management of WireMock servers.
 */
public class WireMockExtensionException extends RuntimeException {
    /**
     * Constructs a new {@code WireMockExtensionException} with a message.
     * @param message the message
     */
    public WireMockExtensionException(String message) {
        super(message);
    }

    /**
     * Constructs a new {@code WireMockExtensionException} with a message and a cause.
     * @param message the message
     * @param cause the cause
     */
    public WireMockExtensionException(String message, Throwable cause) {
        super(message, cause);
    }
}
