package uk.co.paulbenn.wiremock;

import com.github.tomakehurst.wiremock.WireMockServer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates an explicitly declared field of type {@link WireMockServer} as <b>named</b>. Once a server is named, its
 * lifecycle can be managed by the extension.
 *
 * <p>Fields of type {@code WireMockServer} that are *not* annotated with {@code Named} are ignored by the extension.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Named {
    /**
     * What to name this server.
     *
     * @return the name of the annotated {@code WireMockServer}
     */
    String value();
}
