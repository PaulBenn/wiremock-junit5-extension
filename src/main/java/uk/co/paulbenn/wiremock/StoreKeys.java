package uk.co.paulbenn.wiremock;

import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * Internally used (and arbitrarily defined) {@link ExtensionContext.Store} keys.
 *
 * <p>Keys are of type {@code String} for convenience only, as {@code Store} maps accept any object.
 */
public final class StoreKeys {

    /**
     * Store key to the map of all currently registered WireMock servers.
     *
     * The stored value will always be of type {@code Map<String, WireMockServer>}.
     */
    public static String KNOWN_SERVERS_KEY = "WM-known-servers";

    private StoreKeys() {

    }

    /**
     * Returns a store key given a WireMock server name.
     *
     * @param serverName the WireMock server's name.
     * @return a custom, store-specific, identifier for this WireMock server.
     */
    public static String wiremockServerKey(String serverName) {
        return "WM-server-" + serverName;
    }
}
