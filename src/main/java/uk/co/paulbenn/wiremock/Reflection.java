package uk.co.paulbenn.wiremock;

import java.lang.reflect.Field;

/**
 * Unchecked reflection utilities.
 */
public class Reflection {

    /**
     * Reflectively reads the value of a given field on a target object and casts it to the caller's requested return
     * type.
     *
     * @param field the field to read
     * @param target the target object on which to read the field
     * @param <T> the return type requested
     * @return the value of the field, cast to the type requested by the caller
     */
    public static <T> T readField(Field field, Object target) {
        boolean accessible = field.canAccess(target);

        try {
            field.setAccessible(true);

            Object rawValue = field.get(target);

            @SuppressWarnings("unchecked")
            T castValue = (T) rawValue;

            return castValue;
        } catch (IllegalAccessException e) {
            throw new WireMockExtensionException("illegal reflective field access - see cause", e);
        } finally {
            field.setAccessible(accessible);
        }
    }

    /**
     * Reflectively writes a new value to a given field on a given object.
     *
     * @param field the field to write to
     * @param target the object on which to write the field
     * @param value the value to write
     */
    public static void writeField(Field field, Object target, Object value) {
        boolean accessible = field.canAccess(target);

        try {
            field.setAccessible(true);

            field.set(target, value);
        } catch (IllegalAccessException e) {
            throw new WireMockExtensionException("illegal reflective field access, see cause", e);
        } finally {
            field.setAccessible(accessible);
        }
    }
}
